README

Setting up the environment

 - This app was developed using Python 3.5.1, on a Linux platform.
   - Tested on Python 3.5.1 and Python 3.4.
 - In addition to Python, the app requires:
   - lxml (v3.6.0) for html parsing
   - cssselect (v0.9.1) to support html parsing
 - To install these dependencies, execute: `pip install -r requirements.txt`

Running the app

At the terminal/command prompt, execute:

`python app.py`


Running the tests

At the terminal/command prompt, execute:

`python tests.py`

