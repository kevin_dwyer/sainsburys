#! /usr/bin/env python
"""Sainsbury's Software Engineer Technical Test."""

import json
import math
import re
import sys
import urllib.request

import lxml.html
import lxml.cssselect


BASE_URL = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html'
RESULT_KEYS = ['description', 'size', 'title', 'unit_price']


def extract_titles(html, cssselector='.productInfo a'):
    """
    Extract product titles from an lxml.etree.ElementTree.

    Arguments:
    html -- the element from which the titles will be extracted

    Keyword agruments:
    cssselector -- the css selector used to locate the titles
    """
    elements = extract_selected_elements(html, cssselector)
    return get_texts(elements)


def extract_unit_prices(html, cssselector='.pricePerUnit'):
    """
    Extract product per unit prices from an lxml.etree.ElementTree.

    Arguments:
    html -- the element from which the prices will be extracted

    Keyword arguments:
    cssselector -- the css selector used to locate the titles
    """
    elements = extract_selected_elements(html, cssselector)
    texts = get_texts(elements)
    return [float(re.search(r'\d*[.]?\d{2}', x).group(0)) for x in texts]


def extract_description_links(html, cssselector='.productInfo a'):
    """
    Extract links to pages containing product descriptions from an
    lxml.etree.ElementTree.

    Arguments:
    html -- the element from which the prices will be extracted

    Keyword arguments:
    cssselector -- the css selector used to locate the titles
    """
    elements = extract_selected_elements(html, cssselector)
    return [x.attrib['href'] for x in elements]


# The spec is not clear about which description should be extracted
# from the product information page.  Candidates are:
# - the meta description tag's content
# - the product title description
# - the product data item description
#
# Since the final two are both trivial to extract, and this is a
# test, let's assume that the meta description is what's required.
# In real life, we'd seek clarification.


def extract_description(html, cssselector='meta'):
    """
    Extract the content of the meta description tag from an lxml.etree.ElementTree.

    Arguments:
    html -- the element from which the prices will be extracted

    Keyword arguments:
    cssselector -- the css selector used to locate the titles
    """
    elements = extract_selected_elements(html, cssselector)
    meta_description = next(x for x in elements if x.attrib.get('name') == 'description')
    return meta_description.attrib['content']


def fetch_info_pages(links):
    """
    Fetch the pages at the provided links and return their content.

    Arguments:
    links -- a collection of links to retrieve.
    """
    # Use urlopen here instead of just using lxml's
    # parse function because we want to get a count
    # of the bytes in the pages, and parsing then
    # serialising using lxml doesn't always return
    # identical html.
    pages = []
    for link in links:
        with urllib.request.urlopen(link) as resp:
            pages.append(resp.read())
    return pages


def format_sizes(pages):
    """
    Returns sizes of pages formatted in kilobytes.

    Uses the SI definition of kB, 1000 bytes = 1 kilobyte.

    Arguments:
    pages -- a collection of strings
    """
    sizes = [len(x) / 1000 for x in pages]
    return ['{:.1f}kb'.format(x) for x in sizes]


def merge(keys, values):
    """
    Merges keys and values into a list of dicts.

    Arguments:
    keys -- a list of key names
    values -- a list of lists, each list is the values of a particular kind.
              All the lists should be of equal length.
              The length of the values list should equal the length of the keys list.
              The values list should be ordered by kind, to match the corresponding keys.
    """
    return [dict(zip(keys, x)) for x in zip(*values)]


def extract_selected_elements(html, cssselector):
    """
    Extract elements matching cssselector from an lxml.etree.ElementTree.

    Arguments:
    html -- the element from which the prices will be extracted

    Keyword arguments:
    cssselector -- the css selector used to locate the titles
    """
    selector = lxml.cssselect.CSSSelector(cssselector)
    return selector(html)


def get_texts(elements):
    """
    Get the text attributes of the provided elements, stripped of whitespace.

    Argeuments:
    elements -- the elements to be processed.
    """
    return [x.text.strip() for x in elements]


def main():
    """Main function for the script."""
    html = lxml.html.parse(BASE_URL)
    titles = extract_titles(html)
    unit_prices = extract_unit_prices(html)
    description_links = extract_description_links(html)
    info_pages = fetch_info_pages(description_links)
    descriptions = [extract_description(lxml.html.fromstring(x)) for x in info_pages]
    sizes = format_sizes(info_pages)
    result_data = merge(RESULT_KEYS, [descriptions, sizes, titles, unit_prices])
    total = math.fsum(x for x in unit_prices)
    output = {
        'results': result_data,
        'total': total
    }
    json.dump(output, sys.stdout)
    return


if __name__ == '__main__':
    sys.exit(main())
