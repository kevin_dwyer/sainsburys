"""Unit tests for Sainsbury's Software Engineer Technical Test."""

import unittest

import lxml.html

import app


class ExtractTitlesTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.html = lxml.html.parse('fixtures/mainpage.html')

    def test_extracts_product_titles_from_html(self):
        expected = ["Sainsbury's Avocado, Ripe & Ready x2", "Sainsbury's Avocados, Ripe & Ready x4"]
        titles = app.extract_titles(self.html)
        self.assertEqual(expected, titles)


class ExtractUnitPricesTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.html = lxml.html.parse('fixtures/mainpage.html')

    def test_extracts_unit_prices_from_html(self):
        expected = [1.80, 3.20]
        unit_prices = app.extract_unit_prices(self.html)
        self.assertEqual(expected, unit_prices)


class ExtractDescriptionLinksTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.html = lxml.html.parse('fixtures/mainpage.html')

    def test_extracts_description_links_from_html(self):
        expected_root = 'http://hiring-tests.s3-website-eu-west-1.amazonaws.com'
        expected = [
            expected_root + '/2015_Developer_Scrape/sainsburys-avocado--ripe---ready-x2.html',
            expected_root + '/2015_Developer_Scrape/sainsburys-avocados--ripe---ready-x4.html']
        links = app.extract_description_links(self.html)
        self.assertEqual(expected, links)


class ExtractDescriptionTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.html = lxml.html.parse('fixtures/descriptionpage.html')

    def test_extracts_description_from_html(self):
        expected = ("Buy Sainsbury's Apricot Ripe & Ready x5 online from Sainsbury's, the same great quality, "
                    "freshness and choice you'd find in store. Choose from 1 hour delivery slots and collect "
                    "Nectar points.")
        description = app.extract_description(self.html)
        self.assertEqual(expected, description)


class ComputeSizesTestCase(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        with open('fixtures/mainpage.html') as f:
            cls.html = f.read()

    def test_formats_sizes_correctly(self):
        expected = ['10.7kb']
        pages = [self.html]
        sizes = app.format_sizes(pages)
        self.assertEqual(expected, sizes)


class MergeTestCase(unittest.TestCase):

    def test_merges_values_into_dicts(self):
        expected = [
            {
                'title': 'title1',
                'size': '10.7kb',
                'unit_price': 1.80,
                'description': 'description1'
            },
            {
                'title': 'title2',
                'size': '45.6kb',
                'unit_price': 3.20,
                'description': 'description2'
            }
        ]
        keys = ['title', 'size', 'unit_price', 'description']
        titles = ['title1', 'title2']
        sizes = ['10.7kb', '45.6kb']
        prices = [1.80, 3.20]
        descriptions = ['description1', 'description2']
        values = [titles, sizes, prices, descriptions]
        merged = app.merge(keys, values)
        self.assertEqual(expected, merged)


if __name__ == '__main__':
    unittest.main()
